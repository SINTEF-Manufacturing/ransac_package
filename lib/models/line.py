# coding=utf-8

"""
Line model for RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import dataclasses

import numpy as np

from . import RanSaCModel


class LineModel(RanSaCModel):
    """Model for RanSaC'ing for lines."""

    cardinality = 2
    
    def __init__(self, point, direction):
        self.p = np.array(point)
        self.ud = np.array(direction) / np.linalg.norm(direction)
        self.dim = self.p.shape[0]

    @classmethod
    def new_point_direction(cls, p, d):
        return cls(p, d)

    @classmethod
    def new_random_model(cls,
                         ps: np.ndarray,  # (N,3)-array [m]
                         res_tol: float,  # [m]
                         min_sep: float = 1.0e-3,  # [m]
                         max_attempts: int = 1000,  # []
                         min_length: float = None,  # [m]
                         direction: np.ndarray = None,  # (3,)-array giving line direction
                         dir_tol: float = None  # [rad]
                         ):
        """Select the required number of points from 'ps' at random for
        fitting a model. 'min_sep' devices the minimal acceptable
        distance between the two points selected for representing the
        model. 'max_attempts' denotes the maximum number of attempts
        at finding an allowable model. 'min_length' may be given to
        require a minimum distance between the extremal points on the
        line model points. 'direction' may be given, whereby the model
        is restricted to have a direction at most 'dir_tol' radians
        deviation from 'direction'
        """
        nps, dim = ps.shape
        attempts = 0
        # Initial arguments check on direction
        if direction is not None:
            if dir_tol is None:
                raise Exception(
                    cls.__name__ + '.new_random_model' +
                    ': If given "direction", "dir_tol" must also be given!')
            # Make direction a unit vector
            ud_req = direction / np.linalg.norm(direction)
            # Calculate minimum absolute projection corresponding to
            # angle tolerance
            min_proj = np.cos(dir_tol)
        while attempts < max_attempts:
            attempts += 1
            p0, p1 = ps[np.random.randint(0, nps, 2)]
            displ = p1 - p0  # Model displacement
            # Make checks for requirements, rejecting with continue-statement
            if np.linalg.norm(displ) < min_sep:
                continue
            # Create a trial model
            m = cls(p0, displ)
            if direction is not None:
                # There is a direction requirement
                if abs(ud_req.dot(m.ud)) < min_proj:
                    continue
            if min_length is not None:
                # There is a length requirement
                # Select points
                mask, idxs = m.select_points(ps, res_tol)
                ips = ps[idxs]
                # Get point projections on line
                pips = ips.dot(m.ud)
                if pips.max() - pips.min() < min_length:
                    continue
            # No requirement checks failed, so return m as valid model
            return m
        return None

    @classmethod
    def new_fitted_points(cls, points):
        """Fit the model to the given points. The number of points must be
        two. 'points' must be an array of two row-vectors.
        """
        ps = np.array(points)
        d = ps[1] - ps[0]
        ud = d / np.linalg.norm(d)
        return cls(ps[0], ud)

    @property
    def geo_object(self):
        """Return the underlying geometry object, either a math2d.Line or a
        math3d.Line, depending on dimension.
        """
        if self.dim == 2:
            import math2d.geometry as m2dg
            return m2dg.Line.new_point_dir(self.p, self.ud)
        elif self.dim == 3:
            import math3d.geometry as m3dg
            return m3dg.Line(point_direction=(self.p, self.ud))

    def dists(self, ps):
        """Calculate the model dist to all points in 'ps'."""
        rel_ps = ps - self.p
        proj_ps = rel_ps - np.outer(self.ud, self.ud).dot(rel_ps.T).T
        return np.linalg.norm(proj_ps, axis=1)
