# coding=utf-8

"""
2D Circle model for RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.fairuse.org, morten.lind@sintef.no"
__status__ = "Development"


import itertools
import dataclasses

import numpy as np
import math3d as m3d
import math3d.geometry as m3dg

from . import RanSaCModel


@dataclasses.dataclass
class Circle3DModel(RanSaCModel):
    """3D Circle model for RanSaC'ing."""
    centre: m3d.Vector
    radius: float
    normal: m3d.Vector

    cardinality: int = 3

    def __post_init__(self):
        pass

    @classmethod
    def new_random_model(cls, ps,
                         res_tol: float,  # [m]
                         min_radius=1.0e-3,
                         max_radius=10.0e-3,
                         min_sep=0.1e-3,
                         max_attempts=1000):
        """Select the required number of points from 'ps' at random for
        fitting a model. 'min_sep' devices the minimal acceptable
        distance between the two points selected for representing the
        model. 'max_attempts' denotes the maximum number of attempts
        at finding an allowable model.
        """
        nps, dim,  = ps.shape
        if dim != 3:
            raise Exception(
                'Circle3DModel.new_random_model: '
                + 'Dimension must be 3 in row vectors. '
                + f'Got {dim}')
        m = None
        attempts = 0
        while m is None and attempts < max_attempts:
            attempts += 1
            mps = ps[np.random.choice(range(nps), 3, replace=False)]
            sep_ok = True
            for p1, p2 in itertools.combinations(mps, r=2):
                if np.linalg.norm(p1-p2) < min_sep:
                    sep_ok = False
                    break
            if sep_ok:
                m = cls.new_fitted_points(mps)
                if m.radius < min_radius or m.radius > max_radius:
                    m = None
        return m

    @classmethod
    def new_fitted_points(cls, ps: np.ndarray):
        """Select from ps, shape (n, dim), a set of points at random for
        forming a model.
        """
        ps = np.array(ps)
        nps, dim = ps.shape
        if dim != 3:
            raise Exception(
                'Circle3DModel.new_fitted_points: '
                + 'Dimension must be 3 in row vectors. '
                + f'Got {dim}')
        if nps != 3:
            raise Exception(
                'Circle3DModel.new_fitted_points: Must have three points to fit.')
        # All point pairs form a chord of the model circle. Calculate
        # two chord centres and displacement vectors.
        cc1 = m3d.Vector(0.5 * (ps[0] + ps[1]))
        d1 = m3d.Vector(ps[1] - ps[0])
        cc2 = m3d.Vector(0.5 * (ps[1] + ps[2]))
        d2 = m3d.Vector(ps[2] - ps[1])
        # The normal is the cross product of the chord vectors
        n = d1.cross(d2)
        # Form radial directions and lines
        r1 = d1.rotated(np.pi/2)
        r2 = d2.rotated(np.pi/2)
        l1 = m3dg.Line(cc1, cc1+r1)
        l2 = m3dg.Line(cc2, cc2+r2)
        # The pair of closest points between the chord normals
        # must be the best estimate of the centre.
        nearest = l1.nearest_points(l2)
        c = 0.5 * (nearest[0] + nearest[1]).array
        # Use the average of perimeter distances to the centre as radius
        r = np.average(np.linalg.norm(ps - c, axis=1))
        return cls(c, r, n)

    def dists(self, ps):
        rel_ps = ps - self.centre
        return np.abs(np.linalg.norm(rel_ps, axis=1) - self.radius)
