# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind, SINTEF 2019"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.fairuse.org, morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np

from ransac.models import PlaneModel
from ransac import multi_ransac


def _plane_test_set(n_points=1000, model_frac=0.2, gauss_noise=0.05,
                    models=[PlaneModel([1, 2], [1, 1])]):
    dim = models[0].dim
    planes = []
    nm = int(model_frac * n_points)
    for m in models:
        # Generate nm random points
        rps = np.random.uniform(-10, 10, size=(nm, 3))
        # Project on model
        pps = m.projected_points(rps)
        # Add noise
        mps = pps + np.random.normal(0, gauss_noise,
                                     (nm, dim))
        planes.append([m, mps])
    # Stack all model points
    mps_tot = np.vstack([pl[1] for pl in planes])
    # Find ranges
    mins = [mps_tot[:, d].min() for d in range(dim)]
    maxs = [mps_tot[:, d].max() for d in range(dim)]
    # Add random points
    rps = np.random.uniform(mins, maxs, (n_points, dim))
    # Stack all points
    ps = np.vstack((rps, mps_tot))
    return ps, rps, mps_tot, planes


def test_plane_model(plot=True, regress=False):
    n_points = 1000
    m0s = [PlaneModel([1, 1, 0.0], [1, 1, 1.0])]
    ps, rps, m0ps, plane0s = _plane_test_set(n_points=n_points, model_frac=0.1,
                                             models=m0s)
    t0 = time.time()
    e_models, rem_ps = multi_ransac(ps, n_models=len(m0s),
                                    model_type=PlaneModel,
                                    model_kwargs=dict(min_sep=0.1,
                                                      max_prod=0.2),
                                    min_inliers=0.01*n_points,
                                    n_iter=1000, res_tol=0.05, cons_tol=0.2,
                                    regress=regress, max_unimp_count=100)
    print('Time for ransac\'ing:', time.time() - t0)
    print('Number of fitted points:', [pl.ins.shape[0] for pl in e_models])
    if plot:
        import matplotlib.pyplot as plt
        proj = '3d'
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection=proj, aspect='equal')
        ax.scatter(*rem_ps.T, s=5, c='g', marker='o')
        ax.scatter(*m0ps.T, s=10, c='b', marker='o')
        em = e_models[0]
        ax.scatter(*em.ins.T, s=20, c='r', marker='o')
        ax.scatter(*em.cons.T, s=20, c='r', marker='x')
        plt.show(block=False)
    return e_models


if __name__ == '__main__':
    e_models = test_plane_model()
