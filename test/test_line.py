# coding=utf-8

"""
Test script for line RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time

import numpy as np

from ransac.models import LineModel
from ransac import multi_ransac


def _line_test_set(n_points=1000, model_frac=0.1, gauss_noise=0.025,
                   models=[LineModel.new_point_direction([1, 2], [1, 1])]):
    global nm, mts, m, rps, ps, mpoints_tot
    dim = models[0].dim
    lines = []
    nm = int(model_frac * n_points)
    for m in models:
        mts = np.random.uniform(-1, 1, (nm, 1))
        lpoints = m.ud * mts + m.p + np.random.normal(0, gauss_noise,
                                                      (nm, dim))
        lines.append([m, lpoints])
    mpoints_tot = np.vstack([l[1] for l in lines])
    mins = [mpoints_tot[:, d].min() for d in range(dim)]
    maxs = [mpoints_tot[:, d].max() for d in range(dim)]
    rps = np.random.uniform(mins, maxs, (n_points, dim))
    ps = np.vstack((rps, mpoints_tot))
    return ps, rps, mpoints_tot, lines


def test_line_model(dim=2, plot=True, regress=False):
    global ld0s, ps, rps, lines, e_models, m0s, m, rm
    n_points = 300
    if dim == 2:
        m0s = [LineModel.new_point_direction(p, ud)
               for p, ud in zip([[1.0, 2.0],
                                 [2.0, 2.0],
                                 [1.0, 0.5]],
                                [[1.0, 1.0],
                                 [1.0, 0.0],
                                 [2.0, 1.0]])]
    elif dim == 3:
        m0s = [LineModel.new_point_direction(p, ud)
               for p, ud in zip([[1.0, 2.0, 2.0],
                                 [2.0, 2.0, 3.3],
                                 [1.0, 0.5, 2.0]],
                                [[1.0, 1.0, 1.0],
                                 [1.0, 0.0, 1.0],
                                 [2.0, 1.0, 0.0]])]

    ps, rps, m0ps, line0s = _line_test_set(n_points=n_points, model_frac=0.2,
                                           models=m0s)
    t0 = time.time()
    e_models, rem_ps = multi_ransac(ps, n_models=3, model_type=LineModel,
                                    model_kwargs=dict(min_sep=0.1,
                                                      #direction=[2, 1],
                                                      #dir_tol=np.deg2rad(1.0)
                                                      ),
                                    min_inliers=0.1*n_points,
                                    n_iter=1000, res_tol=0.05, cons_tol=0.1,
                                    regress=regress, max_unimp_count=100)
    print('Time for ransac\'ing:', time.time() - t0)
    print('Number of fitted points:', [ln.ins.shape[0] for ln in e_models])
    # Sort e-lines according to matching of ld0s
    m0s_matched = []
    if len(e_models) > 0:
        des = [el.m.ud for el in e_models]
        for de in des:
            m0s_matched.append(
                m0s[np.argmin([1-np.abs(de.T.dot(m0.ud)) for m0 in m0s])])
    for m0, e in zip(m0s_matched, e_models):
        err_direct = 1-np.abs(m0.ud.dot(e.m.ud))
        err_regress = None if not regress else 1-np.abs(m0.ud.dot(e.rm.ud))
        print('Misalignment estimated {} and regressed {}:'
              .format(err_direct, err_regress))
    if plot:
        import matplotlib.pyplot as plt
        if dim == 2:
            proj = 'rectilinear'
        if dim == 3:
            proj = '3d'
            from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection=proj, aspect='equal')
        ax.scatter(*rem_ps.T, s=5, c='g', marker='o')
        ax.scatter(*m0ps.T, s=40, c='b', marker='o')
        for m0, el in zip(m0s_matched, e_models):
            l0 = np.vstack((m0.p-m0.ud, m0.p+m0.ud))
            le = np.vstack((el.m.p-el.m.ud, el.m.p+el.m.ud))
            ax.scatter(*el.ins.T, s=20, c='r', marker='o')
            ax.scatter(*el.cons.T, s=20, c='r', marker='x')
            ax.plot(*l0.T, c='b', linestyle='-', linewidth=5)
            ax.plot(*le.T, c='r', linestyle='-')
        plt.show(block=False)
    return e_models


if __name__ == '__main__':
    e_models = test_line_model()
